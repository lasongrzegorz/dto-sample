from dataclasses import dataclass
from typing import ClassVar


@dataclass()
class Customer:
    _customer_id: int
    _username: str
    _instances: ClassVar[list] = []

    def __post_init__(self):
        Customer._instances.append(
            {"_customer_id": self._customer_id, "_username": self._username}
        )

    @property
    def customer_id(self):
        return self._customer_id

    @customer_id.deleter
    def customer_id(self):
        del self._customer_id
        del self._username

    @classmethod
    def find_username(cls, username):
        return [
            instance for instance in cls._instances
            if instance["_username"] == username
        ]


if __name__ == "__main__":
    random_db = [
        {"_customer_id": 1, "_username": "Mike"},
        {"_customer_id": 2, "_username": "Greg"},
        {"_customer_id": 3, "_username": "John"},
        {"_customer_id": 4, "_username": "Mike"},
    ]

    customer1 = Customer(**random_db[0])
    customer2 = Customer(**random_db[1])
    customer3 = Customer(**random_db[2])
    customer4 = Customer(**random_db[3])
