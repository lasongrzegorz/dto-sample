import unittest

from main import Customer


class TestCustomer(unittest.TestCase):
    def setUp(self):
        self.customer1 = Customer(1, "Mike")
        self.customer2 = Customer(2, "Greg")
        self.customer3 = Customer(3, "John")
        self.customer4 = Customer(4, "Mike")

    def tearDown(self):
        del self.customer1
        del self.customer2
        del self.customer3
        del self.customer4

    def test_get_customer_id(self):
        self.assertEqual(self.customer1.customer_id, 1)
        self.assertEqual(self.customer3.customer_id, 3)
        self.assertNotEqual(self.customer1.customer_id, 2)
        self.assertNotEqual(self.customer4.customer_id, 1)

    def test_set_customer(self):
        expected_equal = Customer(_customer_id=5, _username="Mike")
        expected_not_equal = Customer(_customer_id=7, _username="Mike")

        self.assertEqual(Customer(5, "Mike"), expected_equal)
        self.assertNotEqual(Customer(6, "Mike"), expected_not_equal)

    def test_find_username(self):
        expected_Mike = [
            {"_customer_id": 1, "_username": "Mike"},
            {"_customer_id": 4, "_username": "Mike"},
        ]
        expected_Greg = [
            {"_customer_id": 1, "_username": "Greg"},
            {"_customer_id": 4, "_username": "Greg"},
        ]
        self.assertCountEqual(Customer.find_username("Mike"), expected_Mike)
        self.assertFalse(Customer.find_username("Mike").__eq__(expected_Greg))


if __name__ == "__main__":
    unittest.main()
